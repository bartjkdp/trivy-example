package main

import (
	"fmt"
	"os"

	dbTypes "github.com/aquasecurity/trivy-db/pkg/types"
	"github.com/aquasecurity/trivy/pkg/report"
	"gitlab.com/commonground/haven/haven/trivy-scan/internal/trivy"
)

func main() {
	results, err := trivy.Scan("owasp/modsecurity")
	if err != nil {
		fmt.Errorf("error occured: %s", err)
	}

	reportSeverities := []dbTypes.Severity{
		dbTypes.SeverityUnknown,
		dbTypes.SeverityLow,
		dbTypes.SeverityMedium,
		dbTypes.SeverityHigh,
		dbTypes.SeverityCritical,
	}

	if err = report.WriteResults("table", os.Stdout, reportSeverities, results, "", false); err != nil {
		panic(err)
	}
}
