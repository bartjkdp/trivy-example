package trivy

import (
	"context"
	"time"

	"github.com/aquasecurity/fanal/cache"
	"github.com/aquasecurity/trivy-db/pkg/db"
	"github.com/aquasecurity/trivy/pkg/log"
	"github.com/aquasecurity/trivy/pkg/report"
	"github.com/aquasecurity/trivy/pkg/types"
	"github.com/aquasecurity/trivy/pkg/utils"
	"golang.org/x/xerrors"
)

const (
	cacheDir = "/tmp/trivy"
)

// Scan a specific image and report results
func Scan(imageName string) (report.Results, error) {
	if err := log.InitLogger(false, true); err != nil {
		return nil, xerrors.Errorf("failed to initialize a logger: %w", err)
	}

	// configure cache dir
	utils.SetCacheDir(cacheDir)
	cacheClient, err := cache.NewFSCache(cacheDir)
	if err != nil {
		return nil, xerrors.Errorf("unable to initialize the cache: %w", err)
	}
	defer cacheClient.Close()

	// Mimic to be Trivy v0.12.0
	if err = DownloadDB("0.12.0", cacheDir, false, false, false); err != nil {
		return nil, err
	}

	if err = db.Init(cacheDir); err != nil {
		return nil, xerrors.Errorf("error in vulnerability DB initialize: %w", err)
	}
	defer db.Close()

	ctx := context.Background()

	// scan an image in Docker Engine or Docker Registry
	scanner, cleanup, err := initializeDockerScanner(ctx, imageName, cacheClient, cacheClient, 60*time.Second)
	if err != nil {
		return nil, xerrors.Errorf("unable to initialize the docker scanner: %w", err)
	}
	defer cleanup()

	scanOptions := types.ScanOptions{
		VulnType:            []string{"os", "library"},
		ScanRemovedPackages: false,
		ListAllPackages:     false,
		SkipFiles:           nil,
		SkipDirectories:     nil,
	}

	results, err := scanner.ScanArtifact(ctx, scanOptions)
	if err != nil {
		return nil, xerrors.Errorf("error in image scan: %w", err)
	}

	vulnClient := initializeVulnerabilityClient()
	for i := range results {
		vulnClient.FillInfo(results[i].Vulnerabilities, results[i].Type)
	}

	return results, nil
}
