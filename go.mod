module gitlab.com/commonground/haven/haven/trivy-scan

go 1.15

require (
	github.com/aquasecurity/fanal v0.0.0-20201028122920-48a25d865182
	github.com/aquasecurity/trivy v0.12.0
	github.com/aquasecurity/trivy-db v0.0.0-20201025093117-4ef51a6e2c4b
	github.com/google/wire v0.4.0
	github.com/spf13/afero v1.2.2
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
	k8s.io/utils v0.0.0-20201005171033-6301aaf42dc7
)
